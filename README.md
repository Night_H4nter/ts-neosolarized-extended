# NeoSolarized Extended for Nvim-treesitter

## Introduction

This is a color scheme inspired by and based on [NeoSolarized](https://github.com/overcache/NeoSolarized)
by Simon "overcache" Lu, which a version of [Solarized](https://ethanschoonover.com/solarized/) color scheme
created by Ethan "altercation" Schoonover, but with true color support. It uses [tree-sitter](https://github.com/nvim-treesitter/nvim-treesitter) as it's
parsing engine, and provides extended (compared to original Solarized) syntax
highlighting.

*Keep in mind this is heavily WIP, and also it will probably never receive some
"additional" features of original Solarized like "readability", since nobody
uses it anyway I believe; and non-gui colors support, because I'm lazy and
there's no reason not to use a true color terminal nowadays (but, if anyone
makes a PR with cterm support, I'll probably merge it).*

Generated with `generate` Ruby script (some older version)
[from here](https://github.com/felipec/vim-felipec).

# Getting started

## Requirements:
1. Neovim-nightly (nvim-treesitter requirement as of early 2021)
2. Nvim-treesitter

## Installation
1. Install Neovim-nightly 
2. Install Nvim-treesitter
3. Install TSNeoSolarizedExtended

If you use `vim-plug` add these lines into your `init.vim` (you can make
something up for other plugin managers and manual installation quite easily):

    Plug 'nvim-treesitter/nvim-treesitter'
    Plug 'https://gitlab.com/Night_H4nter/ts-neosolarized-extended.git'
    "the order is important, otherwise it won't work
    colorscheme ts-neosolarized-ext
    
*I don't know why does it not respect `colors_name`. I'd appreciate help on
this but it's not the priority for me now.*

# Screenshots
C syntax sample with NeoSolarized

![C syntax with regular NeoSolarized](./screenshots/c_without_ts.png)

C syntax with ts-neosolarized-ext

![C syntax with tree-sitter-powered and extended NeoSolarized](./screenshots/c_with_ts.png)

# TODO (from the most prioritized to the least):
1. Finish the highlighting for other languages
2. Make other contrast options
3. Fix minor issues
